var kue = require('kue') 
  , OAuth= require('oauth').OAuth
  , OAuth2= require('oauth').OAuth2
  , conf = require('conf')
  , log = require('logger').log
  , coll_user = require('mongoskin').db('localhost/my_database2').collection('user')  // actually this needs to be the user object
  , FacebookClient = require("facebook-client").FacebookClient
  , socialanimal = require('socialanimal')
  , sa_twitter = require('socialanimal-twitter')
  ;

var https = require('https');  // used by facebook module

// http://blog.joeandrieu.com/2012/01/30/the-worlds-simplest-autotweeter-in-node-js/
var twitterer = new OAuth(
       "https://api.twitter.com/oauth/request_token",
       "https://api.twitter.com/oauth/access_token",
       conf.twit.consumerKey,
       conf.twit.consumerSecret,
       "1.0",
       null,
       "HMAC-SHA1"
);

// create our job queue

var jobs = kue.createQueue();

/*
* posting something on twitter
    var job = jobs.create('twitter_send_twitt', {
        title: 'sending twitter',
        body: {status: 'sending twitter - test'}, 
        token: providers.twitter.token, 
        tokenSecret: providers.twitter.tokenSecret,
        user: 1
    });
* 
*/
jobs.process('twitter_send_twitt', 1, function(job, done){
  job.log('twitter processing', job.data.body);
  twitterer.post("http://api.twitter.com/1/statuses/update.json", job.data.token, job.data.tokenSecret, job.data.body, "application/json",
     function (error, data, response2) {
     if(error){
       log.debug('Error: Something is wrong.\n'+JSON.stringify(error)+'\n');
       for (i in response2) {
           out = i + ' : ';
         try {
         out+=response2[i];
         }  catch(err) {}
           out += '/n';
//           log.debug(out);
         }
       }else{
           log.debug('Twitter status updated.\n');
       }
         });
  done();
});

// test twitter 
function twitter_test() { 
  function create() {
    coll_user.find({'providers.twitter': {'$exists': true}}).each(function(err, result)  { 
      if(result && result['providers']) { 
        console.log('result', result.providers.twitter);
        var job = jobs.create('twitter_send_twitt', {
            title: 'sending twitter',
            body: {status: 'sending twitter - test'}, 
            token: result.providers.twitter.token, 
            tokenSecret: result.providers.twitter.tokenSecret,
            user: 1
        });

        job.on('complete', function(){
            log.debug(" Job complete");
        }).on('failed', function(){
            log.debug(" Job failed");
        }).on('progress', function(progress){
          process.stdout.write('\r  job #' + job.id + ' ' + progress + '% complete');
        });

        job.save();

      //  setTimeout(create, Math.random() * 2000 | 0);
    }
    });
  }
  create();
}


/*
* posting something on facebook
* 
  var job = jobs.create('facebook_post', {
      title: 'sending fb',
      body: 'test', 
      facebook_id: result.providers.facebook.id, 
      accessToken: result.providers.facebook.accessToken,
      user: 1
  });
*/
jobs.process('facebook_post', 1, function(job, done){
  job.log('facebook processing', job.data.body);

  var options = {
      host: 'graph.facebook.com',
      port: 443,
      path: '/'+job.data.facebook_id+'/feed?access_token='+job.data.accessToken,
      method: 'POST',
      headers: { 'message': job.data.body }
  };

  var req = https.request(options, function(res) {
      console.log("statusCode: ", res.statusCode);
      console.log("headers: ", res.headers);

      res.on('data', function(d) {
          process.stdout.write(d);
      });
  });
  log.debug('done');

  req.end();
  done();
});

var facebook_test = function () { 
  coll_user.find({'providers.facebook': {'$exists': true}}).each(function(err, result)  { 
    if(result) { 

      var job = jobs.create('facebook_post', {
          title: 'facebook - postingwall for user ' + result.providers.facebook.name,
          body: 'test', 
          facebook_id: result.providers.facebook.id, 
          accessToken: result.providers.facebook.accessToken,
          user: 1
      });

      job.on('complete', function(){
          log.debug(" Job complete");
      }).on('failed', function(){
          log.debug(" Job failed");
      }).on('progress', function(progress){
        process.stdout.write('\r  job #' + job.id + ' ' + progress + '% complete');
      });

      job.save();
   }
  });
}

// start the UI
var TEST = true; 
kue.app.listen(8000);
if (TEST) { 
//  twitter_test();
  facebook_test();
} 
log.debug('UI started on port 8000');
