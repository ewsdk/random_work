var Backbone = require('backbone')
  , mongo = require('mongoskin')
  , db = mongo.db('localhost:27017/test?auto_reconnect');

/*

create a real model for the user with the following 

- addPRovider(provider, providerdata) 
- showProviders 
- save 
- retrieve(provider, userID) 
*/
 
exports.userModel = Backbone.Model.extend({ 
  operations: 0, 
  defaults: { 
    username: {},
    providers: {},
  }, 
  initialize: function(a) { 
  },
  get_providers: function() {
    return _und.keys(this.providers);
  },
  get_from_provider: function(user, callback) { 

    if(!callback) { callback = function() {} };
    var done = false; 

    var userQuery = {};
    userQuery['providers.' + user.provider + '.id'] = user.user_id;   // query to see if we already have a user
                                                               // with the same credentials  
    var that=this;
    // mutex 
    this.operations++; 
    db.collection('users').findOne(userQuery, function(err, post) { 
      if(post) { 
        that.set(helpers.extend({}, post));
        done = true; 
        that.operations--;
        callback();
      } else { 
        // user does not exist, inserting
          // are we adding a completely new user? or just adding information to an existing user? 
          newUser = {'username': user.username, providers: {}  } 
          newUser.providers[user.provider] = helpers.extend({}, user);  // deep copy 

          console.log('user does not exist, inserting', newUser);
          db.collection('users').insert(newUser, function(err, doc) { 
            req.logIn(newUser, function(err) {
              if (err) { return next(err); }
              return res.redirect('/');
            });
          });

      } 
    });

    return this;
  },

  set_provider: function(provider, data) { 
    this.providers[provider] = data; 
    return this;
  },
  save: function(callback) { 
    if(!callback) { callback = function() {} };
    console.log('this', this);
    db.collection('users').update({'_id': ObjectID.createFromHexString(this._id)}, this.attributes, {safe: true}, function(err, doc) { console.log('updated', err, doc); callback() });
    return this;
  }, 
  /* 
    Wait until r/w operations to db are done, for sync purposes 
    TODO : this doesn't work, figure out something
  */
  wait_sync: function(callback) { 
    while(! this.operations == 0 ) { 
      setTimeout(function() {}, 100);
      console.log('waiting', this.operations);
    };
    callback();
    return this
  } 
});

var user = new exports.userModel();
console.log('user', user);


