var User = require("./user.js").User;
var user = require("./user.js");
var Relationship = require("./relationship.js").Relationship;
var EventEmitter = require("events").EventEmitter
var inherits = require('util').inherits;

inherits(user, EventEmitter);

//var item = require("./item.js");
//var Item = require("./item.js").Item;

//exports.Item = Item;

exports.User = User;

// see docs.json

exports.createUser   = function (opts, on) { return new User(opts, on); };
exports.retrieveUser = function (opts, attrs, cb) { user.retrieve(opts, attrs, cb); };
exports.retrieveOrCreate = function (opts, userData, attrs, cb) { user.retrieveOrCreate(opts, userData, attrs, cb); };
exports.updateUser   = function (this_user, attrs) { user.update(this_user, attrs) };

//exports.addProvider = function(user, provider, providerInfo) { ... } ; 
//exports.retrieveFromProvider = function(userID, provider) { ... } ; 

exports.createRelationship = function (from, to, type, on) { return new Relationship(from, to, type, on); }
exports.getOutboundRelationships = function (userID, type, cb) { user.getOutboundRelationships(userID, type, cb) };

