//var neo4j = require('neo4j'),
//    db = new neo4j.GraphDatabase('http://localhost:7474'),
var  neo4j = require('neo4j');
    console.log('neo4j', neo4j);

var  db = new neo4j.GraphDatabase('http://localhost:7474'),
    inherits = require('util').inherits,
    extend = require('./circl-util').extend,
    getObjectClass = require('./circl-util').getObjectClass,
    uuid = require('node-uuid'),
    EventEmitter = require("events").EventEmitter,
    async = require('async'),
    util = require('util');

function Relationship (from, to, type) {

  console.log('relationship', from, to, type);

  var oob = this;

  var fromn, ton;

  extend(oob, {
    type: type,
    from: null,
    to: null 
  });

  if (!type) { oob.emit('error', "Relationship must be instantiated with a type"); return; }

  // ensure both items have nodes in the graph...
  async.waterfall([
    function(cb) {
      async.parallel([
        function(pcb) { _getOrCreateNeoNode(from, pcb) },
        function(pcb) { _getOrCreateNeoNode(to, pcb) }
      ], function(err, res) {
         cb(err, res[0], res[1]);
      });
    },
    function(fromnode, tonode, cb) {
      oob.from = fromnode;
      oob.to = tonode;
// XXX : not saving real nodes, but just types 
//      console.log('from to', oob,from, oob.to);

      // write to the graph store
      oob._graphStore(function(rel) {
        oob.emit('graphstore', rel)
      });
      cb();
    }
  ]);

  return oob;

};

inherits(Relationship, EventEmitter);
exports.Relationship = Relationship;

var _getOrCreateNeoNode = function(node, cb) {

  // it has a graphid so it's in the graph. fetch the node
  // and call cb
  // ok , this is so fucking wrong I can't even believe it 
  if (node.graphID) {
//    db.getNodeById(1, cb);
    db.getNodeById(node.graphID, cb);
    return;
  }

  // otherwise,
  // this thing doesn't have an id, so it's not in the graph db
  var n = db.createNode({
    type: getObjectClass(node),
    id: node.id
  });


  // go ahead and insert it as a node in the graph, update the item
  // so it knows its graph id, and call the cb with the new node.
  async.series([
    function(wcb) {
      n.save(function () { wcb(null) } );
    },
    function(wcb) {
      node.once("objectstore", function () { wcb(null) });
      node.update({ 'graphID': n.id  });
    }
  ], function(err) {
    cb(err, n)
  });

}

Relationship.prototype._graphStore = function(cb) {

  var to = this.to;
  var from = this.from;
  var type = this.type;

  from.createRelationshipTo(
    to,
    type,
    {},
    cb
  );

};

var getRelationships = function(userid, type) { 
  neo4j.getRelationshipsById(1, callback);    
} 

/*
var retrieve = exports.retrieve = function(id, opts, cb) {


  riak.getClient().get(
    'users',
    id,
    function(err, user, meta) {
      if (err) {
        throw new Error('database error');
      }

      var ret = { id: id };

      if (opts === "complete") { opts = user; } // copy entire object

      for (var i in opts) {
        if (Object.prototype.hasOwnProperty.call(user, i)) {
          ret[i] = user[i]; 
        }
      }

      return new User(ret, { 'instantiation': cb });
    }
  );
};

exports.update = function(user, attrs) {

  retrieve(user.id, "complete", function(orig) {
    for (var a in attrs) {
      orig[a] = attrs[a];
    }

    orig.on('objectstore', function () { user.emit('objectstore', orig); });

    orig._objectStore();
  });
};
*/
