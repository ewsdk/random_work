// utility functions

exports.extend = function (dst, src) {
  for (var p in src) { if (Object.prototype.hasOwnProperty.call(src, p)) {
    dst[p] = src[p];
  }}
  return dst;
};

// thx http://blog.magnetiq.com/post/514962277/finding-out-class-names-of-javascript-objects
// Returns the class name of the argument or undefined if
//   it's not a valid JavaScript object.
exports.getObjectClass = function getObjectClass(obj) {
  if (obj && obj.constructor && obj.constructor.toString) {
    var arr = obj.constructor.toString().match(
      /function\s*(\w+)/);
      if (arr && arr.length == 2) {
        return arr[1];
      }
    }

  return undefined;
}
