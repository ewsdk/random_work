var neo4j = require('neo4j'),
    db = new neo4j.GraphDatabase('http://localhost:7474'),
    inherits = require('util').inherits,
    extend = require('./circl-util').extend,
    uuid = require('node-uuid'),
    EventEmitter = require("events").EventEmitter,
    util = require('util'),
    riak = require('riak-js');


function Item (item, on) {

  var ob = this;
  var write = false;

  if (!item.id) {
    write = true;
    item.id = uuid.v4();
  }

  // arrange events
  if (on) {
    for (e in on) {
      ob.on(e, on[e]);
    }
  }

  extend(ob, {
    id: item.id,
    headline: item.headline,
    body: item.body,
    user: item.user // this should be a uuid
  });

  if (write) {
    if (!item.headline) { this.emit('error', "Item must be instantiated with a headline"); return; }
    if (!item.body ) { this.emit('error', "Item must be instantiated with a body"); return; }
    if (!item.user ) { this.emit('error', "Item must be instantiated with a user"); return; }

    ob._objectStore(function() { ob.emit('instantiation', ob) });

    // XXX pull user from riak, get user's neo url
    // XXX create relationship between user and item in neo

  } else {
    ob.emit('instantiation', ob);
  }
  
};

inherits(Item, EventEmitter);

exports.Item = Item;

Item.prototype._objectStore = function(cb) {

  var ob = this;

  riak.getClient().save(
    'items',
    ob.id,
    {
      'body': ob.body,
      'headline': ob.headline,
      'user': ob.user
    }, 
    {
      debug: true,
    },
    function(err) {
       if (err) { console.log(err); throw new Error("database error"); }
       ob.emit('objectstore', ob);
       if (cb) cb();
    }
  );

};

var retrieve = exports.retrieve = function(id, opts, cb) {

  console.log("in item retrieve");

  riak.getClient().get(
    'items',
    id,
    { debug: true },
    function(err, item, meta) {
      if (err) {
        console.log(err);
        throw new Error("database error");
      }

      var ret = { id: id };

      if (opts === "complete") { opts = item; } // copy entire object

      for (var i in opts) {
        if (Object.prototype.hasOwnProperty.call(item, i)) {
          ret[i] = item[i]; 
        }
      }

      return new Item(ret, { 'instantiation': cb });
    }
  );
};

// update instance method.
Item.prototype.update = function(attrs) {
  var ob = this;
  update(ob, attrs, cb);
}

// API-callable update
var update = exports.update = function(item, attrs) {

  // pull version in db
  retrieve(item.id, "complete", function(dbcopy) {

    // update new attributes
    // both in db copy and local copy
    for (var a in attrs) {
      dbcopy[a] = attrs[a];
      item[a] = attrs[a];
    }

    // tie the db object's "i'm done" event to the local copy
    dbcopy.on('objectstore', function () { item.emit('objectstore', item); });

    // go off and do the update
    dbcopy._objectStore();
  });
};

