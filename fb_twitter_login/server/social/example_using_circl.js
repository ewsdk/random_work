var circl = require("./circl");
var async = require("async");
var user0;
var user1;
var rel0;
var getObjectClass = require('./circl-util').getObjectClass;

exports.loads = function(test) {
  test.expect(1);
  var rel = circl.Relationship;
  test.ok(true, rel);
  test.done();
};

exports.creates = function(test) {
  test.expect(1);
  async.waterfall([

    // create a couple of users
    function(cb) {
      async.parallel([
        function (pcb) { 
          user0 = circl.createUser(
            { display: "joshua" }
          );
          user0.on('objectstore',
            function(){ pcb(null) }
          ); 
        },
        function (pcb) { 
          user1 = circl.createUser(
            { display: "bailey" }
          );
          user1.on('objectstore',
            function(){
              pcb(null)
            }
          ); 
        }
      ], function(err) { cb(); } );
    }, 

    // and, relate those users
    function(cb) {
      rel0 = circl.createRelationship(
        user0,
        user1,
        "follow"
      );
      rel0.once("graphstore", function (err) {
        test.equal(err, undefined);
        cb();
        test.done();
      });
    }
  ]);
}

