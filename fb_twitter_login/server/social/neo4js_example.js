var neo4j = require('neo4j');
var db = new neo4j.GraphDatabase('http://localhost:7474');

function callback(err, result) {
    if (err) {
        console.error("ERROR", err);
    } else {
        console.log("RESULT", result);    // if an object, inspects the object
    }
}

var node = db.createNode({hello: 'world'});     // instantaneous, but...
node.save(callback);    // ...this is what actually persists it in the db.

db.getNodeById(1, function(erro, node) { console.log('a', node)});
db.getRelationshipById(1, callback);
