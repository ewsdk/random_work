var inherits = require('util').inherits,
    extend = require('./circl-util').extend,
    uuid = require('node-uuid'),
    EventEmitter = require("events").EventEmitter,
    util = require('util'),
    async = require('async'),
    mongo = require('mongoskin'),
    helpers = require('helpers'),
    conf = require('conf'),
    log = require('logger').log,
    _und = require('underscore'),
    ObjectID = require('mongodb').ObjectID;

var coll_user = mongo.db(conf.mongo.db_url).collection('user');
var coll_social_network = mongo.db(conf.mongo.db_url).collection('social');
var coll_third_party_user_info = mongo.db(conf.mongo.db_url).collection('third_party_users');

/* 

relationships 

{from, to, type, datetime} 
{from : {'twitter' : 1234} , to: { twitter: 345 }, type : 'twitter_follows', datetime: now()} 
type : 

twitter_follows
fb_follows
follows (general out app follows semantics) 
blocks 

adding relationships 

coll_social_network.insert(from: { twitter: $TWITID_FROM}, to: { twitter: $TWITTID_TO}, type: twitter_follows, datetime: now()} 

note: it may not be needed to add {twitter: 1234} , just 1234, but we may try to prevent collissions with userids and ill defined semantics
actually, better this way, so we can model dynamics such as a real user (out user) is following people on twitter

questions : 

1. do we add FULL twitter information to that from or to object? as a demo we can do it, but we are replicating a shit ton of work 

create node: 
  we may also have a different column and add twitter_user information 
  fields we need: 
  userid, provider, username, name, pictu_url using portable contact

  AND (important) a flag to wether this is a user or not, if it's a user, populate the object with userID information right away , or at least userID so we can look at userID 
  this is important because when users join they want to know who is in the system already 

  coll_third_party_user.insert({ ..... })  // we add the twitter user
  
  but this is handled from the crawler, since relationships on twitter only give us userid -> userid, not full userobject

coll_social_network.insert() // add relationships

operations

1. user A joins , he is connected to B, who is on the system, show that 
2. user A joined months ago, he was connected to B, B joins, notify A 

*/ 

var User = function(data) { 
  this.user = data; 
  this.populate = populate;
  this.addProvider = addProvider;
  this.removeProvider = removeProvider;
  inherits(this, EventEmitter);
} 

// we either here take a callback or return events, see wtf
// way more elegant with events

var populate = User.prototype.populate = function(provider, userID, userData, cb) {
  console.log('hi!')

  var self=this;
  var userQuery = {};

  userQuery['providers.' + provider + '.id'] = userID;   // query to see if we already have a user

  coll_user.findOne(userQuery, function(err, user) { 
    if (err) {
      throw new Error('database error');
    }

   if (user) { 
    // TODO : add userData if it is different
    console.log('hi we have an user', user);
    var ret = user;
    ret = helpers.extend({}, self);  // deep copy 
    //self.emit('logged_in', user);
    self.emit('new_user', user);
//    if(cb) cb(ret);
//    return ret;

  } else { 
    // create new user
    var username = userData.username || userData.displayName // TODO : make sure this is not repeated

//    var newUser = {'_id': uuid.v4(), username: username, providers: {}};
    var newUser = {username: username, providers: {}};

    newUser.providers[provider] = helpers.extend({}, userData);  // deep copy 
    newUser = helpers.extend({}, setUserDefaultInformation(newUser));

    coll_user.insert(newUser, function() {
         //ob.emit('objectstore', ob);
         console.log('inserting')
  //       self.emit('new_user', newUser);
         if (cb) cb(newUser);
     });

  } 
 })
};

var addProvider = User.prototype.addProvider = function(provider, user) { 
    // need to check which information is exposed on this, if it exposes all the methods and shit, it will be way more difficult
    var self = this;

    // try to find user with existing provider information 
    // query will have a mongodb query in the form of  {'providers.twitter.id' : '1234' } 

    // here we are trying to find ways to find unlogged users which share credentials with logged one
    // that is: I log on FB, log out, log on twitter, then add facebook, I need to merge both accounts
    // it doesn't work (just deletes everything) 
    // but I put it on TODO

    var query = {};
    query['providers.' + provider + '.id'] = user.id; 
    coll_user.findOne(query, function(err, doc) { 
      if(doc) { 
        log.debug('existing user', doc, query);
        // merge doc information with req.user
        // delete doc 
        // we are losing old userID here  
        // TODO : proper merging here, we just delete the first user (naive solution) 
        // TODO : this log.debug('removed previous user') should be a call to a next() function which will merge this new provider with req.user
        // just like we do below 
        // also this if(doc) should have an else that calls that next() function 
        //coll_user.remove({'_id': doc._id}, {safe: true}, function() { log.debug('removed previous user') });
        self.emit('existing_user', self.user);
      }  

    // there is no user with that information previously on the db, we just merge with the existing 
    var newProvider = user.provider; 
    delete user.provider; 

    self.user['providers'] = self.user['providers'] ? self.user.providers : {};  
    self.user.providers[newProvider] = helpers.extend({}, user);
  
    // we can't update when we have an _id on the update object, so we need to copy it, protect it and remove it 
    var newUser = helpers.extend({}, self.user); // deep copy 

    delete newUser._id;

    // HERE (TODO) we need to call some heuristics to determine
    // which information snippets of the new provider go into the main user information (name, userid, picture) 

    coll_user.update({'_id': ObjectID.createFromHexString(self.user._id)}, newUser, {safe: true}, function(err, doc) { log.debug('updated', err, doc) });
    self.emit('new_provider', self.user);
    console.log('I will add ', provider, ' to ', this);
  });
}

var removeProvider = User.prototype.removeProvider = function(provider) { 
  var self=this;

  // need to check which information is exposed on this, if it exposes all the methods and shit, it will be way more difficult
  console.log('I will remove ', provider, ' to ', self);

  if(_und.keys(self.user.providers).length > 1) { 
    if (self.user.providers[provider]) { 
      delete self.user.providers[provider];
      // here is tricky
      var newUser = helpers.extend({}, self.user); // deep copy 
      delete newUser._id;
      coll_user.update({'_id': ObjectID.createFromHexString(self.user._id)}, newUser, {safe: true}, function(err, doc) { 
        log.debug('updated', err, doc) ; 
        self.emit('provider_removed', self.user) 
      });
  } else { 
    self.emit('no_provider', self.user);
  } 
  } else { 
    self.emit('last_provider', self.user);
  }
} 
User.prototype._objectStore = function(cb) {

  console.log('hi from _objectStore', cb);
  var ob = this;

  var username = ob.username || ob.displayName // TODO : make sure this is not repeated

  var newUser = {'_id': ob._id, username: username, providers: {}};

  newUser.providers[ob.provider] = helpers.extend({}, ob);  // deep copy 
  newUser = helpers.extend({}, setUserDefaultInformation(newUser));

  coll_user.insert(newUser, function() {
       //ob.emit('objectstore', ob);
       console.log('inserting')
       ob.emit('newUser', ob);
       if (cb) cb();
   });
};


// NOTE: implement getOutboundRelationships by provider

function getOutboundRelationships(userID, type, callback) { 
  coll_social_network.find({to: userID, type: type}, callback);
} 

function getInboundRelationships(userID, type, callback) { 
  coll_social_network.find({from: userID, type: type}, callback);
} 

function getFollowing(userID, callback) { 
  getOutboundRelationships(userID, 'follows', callback);
} 

function getFollowers(userID, callback) { 
  getInboundRelationships(userID, 'follows', callback);
} 

function getRelationship(u1, u2, callback) { 
  coll_social_network.find({from : u1, to: u2},  function(data) { 
      if (data && data['type']) {
        callback(data['type']) 
     } else {
      callback() }
    });
} 

function setUserDefaultInformation(user) { 
  if(! user['providers'] || user['providers'].length < 1) { return user }
  var interestingValues = ['username', 'locale', 'picture_url'];
  for (provider in user['providers']) { 
    for (value in interestingValues) { 
      if(user[interestingValues[value]] == undefined  && user['providers'][provider][interestingValues[value]]) { 
        user[interestingValues[value]] = user['providers'][provider][interestingValues[value]];
      } 
    } 
  } 
  return user; 
} 

var getUserInfo = exports.getUserInfo = function(userID, cb) { 
  coll_user.findById(userID, cb);
} 

var getUserInfoByUsername = exports.getUserInfoByUsername = function(username, cb) { 
  console.log('searching', username);
  coll_user.findOne({username: username}, cb);
} 

// update instance method.
User.prototype.update = function(attrs) {
  var ob = this;
  console.log("prototype update time");
  update(ob, attrs);
}


// API-callable update
var update = exports.update = function(user, attrs) {

  console.log("i have been asked to update");

  // pull version in db
  retrieveById(user._id, "complete", function(dbcopy) {

    // update new attributes
    // both in db copy and local copy
    for (var a in attrs) {
      dbcopy[a] = attrs[a];
      user[a] = attrs[a];
    }

    // tie the db object's "i'm done" event to the local copy
    dbcopy.on('objectstore', function () { user.emit('objectstore', user); });

    // go off and do the update
    dbcopy._objectStore();
  });
};



var retrieveById = exports.retrieveById = function(id, opts, cb) {
  retrieve({'_id': ObjectID.createFromHexString(id)}, opts, cb);
}

var retrieve = exports.retrieve = function(query, opts, cb) {

  coll_user.findOne(query, function(err, user) { 
    if (err) {
      throw new Error('database error');
    }

   if (user) { 

     var ret = { };

      if (opts === "complete") { opts = user; } // copy entire object

      for (var i in opts) {
        if (Object.prototype.hasOwnProperty.call(user, i)) {
          ret[i] = user[i]; 
        }
      }
      return new User(ret, { 'instantiation': cb });
  } else { 
    return;
  } 
 })
};

var retrieveOrCreate = exports.retrieveOrCreate = function(query, userData, opts, cb) { 
  var ob=this;
  var user = retrieve(query, opts, cb); 
  if (!(user)) { 
    console.log('creating user and shit');
    return new User(userData, { 'newUser': cb });
  } 
  console.log('user exists');
  return user;
} 

inherits(User, EventEmitter);

exports.User = User;
exports.getOutboundRelationships = getOutboundRelationships;
exports.getFollowing = getFollowing;
exports.getFollowers = getFollowers;

