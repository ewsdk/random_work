
var express = require('express')
  , passport = require('passport')
  , mongo = require('mongoskin')
  , ObjectID = require('mongodb').ObjectIA
  , user = require("./social/user_noneo4j.js")

function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) { return next(); }
  res.redirect('/login')
}

function ensureNotLoggedIn(req, res, next) {
  if (! req.isAuthenticated()) { return next(); }
  res.redirect('/')
}


module.exports = function(app) { 

  app.configure(function() {
    app.use(express.static(__dirname + '/public'));
    app.use(express.cookieParser());
    app.use(express.bodyParser());
    app.use(express.methodOverride());
    app.use(passport.initialize());
    app.use(passport.session());
  });

/*
  * Show all my friends, return JSON
  * user has to be logged in  
  */
  app.get('/1/following/:userID?', ensureAuthenticated, function(req, res){ 
    var userID = req.params['userID'] || req.user['_id']
    user.getFollowing(userID, function(data) { 
      res.end(JSON.stringify(data))
    }) 
  })

  app.get('/1/followers/:userID?', ensureAuthenticated, function(req, res){ 
    var userID = req.params['userID'] || req.user['_id']
    user.getFollowers(userID, function(data) { 
      res.end(JSON.stringify(data))
    }) 
  })

  app.get('/1/friendship/:userID', ensureAuthenticated, function(req, res){
    user.getRelationship(req.user['_id'], req.param('userID'), function(data) { 
      res.end(JSON.stringify(data))
    }) 
  });

  app.get('/1/userinfo/byusername/:username', ensureAuthenticated, function(req, res){ 
    user.getUserInfoByUsername(req.param('username'), function(data) { 
      console.log('data', data);
      res.end(JSON.stringify(data))
    }) 
  })

  app.get('/1/userinfo/:userID?', ensureAuthenticated, function(req, res){ 
    if(!req.params['userID']) { 
      res.end(JSON.stringify(req.user))
    } else { 
      user.getUserInfo(req.param('userID'), function(data) { 
        res.end(JSON.stringify(data))
      }) 
    } 
  })

  app.post('/api/follow', ensureAuthenticated, function(req, res){ 
    console.log('NOT IMPLEMENTED')
  })

}
