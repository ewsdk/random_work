var express = require('express')
  , passport = require('passport')
  , util = require('util')
  , hbs = require('hbs')
  , TwitterStrategy = require('passport-twitter').Strategy
  , FacebookStrategy = require('passport-facebook').Strategy
  , GoogleStrategy = require('passport-google-oauth').OAuth2Strategy
  , RedisStore = require('connect-redis')(express)
  , helpers = require('helpers')
  , mongo = require('mongoskin')
  , ObjectID = require('mongodb').ObjectID
  , conf = require('conf')
  , userModel = require('./usermodel.js')
  , backboneio = require('backbone.io')
  ,  _und = require('underscore')
  , my_mongo_middleware = require('./middleware/mongoskin.js')
  , static_object_middleware = require('./middleware/static_object_middleware.js')
  , my_session_middleware = require('./middleware/my_session.js')
  , socialanimal = require('socialanimal')
  , log = require('./logger.js').log
  , user = require("./social/user_noneo4j.js")
  , User = require("./social/user_noneo4j.js").User
  , Relationship = require('./social/relationship.js').Relationship
  ;

/* 

TODO 

every object returned by a logger, on the serializator, needs to be aware of wether user is logged or not, that's it
we need to show every service on the 'account' tabs 

make sure everything is being deepcopies 

*/

var coll_messages = mongo.db(conf.mongo.db_url).collection('messages');
var coll_events = mongo.db(conf.mongo.db_url).collection('events');
var coll_user = mongo.db(conf.mongo.db_url).collection('user');
var coll_twitter_followers = mongo.db(conf.mongo.db_url).collection('twitter');
var coll_twitter_userInfo = mongo.db(conf.mongo.db_url).collection('twitter_userinfo');
var coll_fb = mongo.db(conf.mongo.db_url).collection('fb');
var coll_notifications = mongo.db(conf.mongo.db_url).collection('notifications');

var socialAnimal = { 'twitter': require('socialanimal-twitter').Provider } 

// add friends from twitter 
// uid == user id (current user) 
// extract all friends from twitter using api and add them to neo4j
var addFriendsFromProvider = {'twitter': 
  function(uid, next){ 
    var provider = 'twitter';
    if (typeof next !== 'function') { next = function() {} };
    var social = socialanimal.use(new socialAnimal[provider]({
      user_id:uid
    }));

    // add followers from twitter into the system 
    // 1. check if current user exists on the neo4j db , actually node needs to exist if we created the user, but check nonetheless 
    // 2. for each new node, check if node exists, add if not
    // 3. create relationship between current_user and new twitter user if relationship doesn't exist (actually abstract that last checking part) 
    social.followers(function(error, followers) {
      console.log('followers', followers.length);
        // fromNode is the current user
        // we are only adding provider information here, other information must be added asynchronously 
        var fromNode = {};
        fromNode[provider] = uid; 
      _und.each(followers, function(followerID) { 
        // TODO : check that the relationship doesn't exist previously 
        // toNode is the 'friend' of the user referenced by uid 
        var toNode = {};
        toNode[provider] = followerID;
        // TODO : Relationship needs to find wether relationship exists or not 
        var r = new Relationship(fromNode, toNode, {'provider': provider}, function() { /*console.log('done', followerID) */});
      })
    });
  } 
}

var saveTwitterUserInfo = function(twitterUserId, next){ 
  if (typeof next !== 'function') { next = function() {} };
  var social = socialanimal.use(new socialAnimal['twitter']({
    user_id:twitterUserId 
  }));

  social.userInfo(function(error, data) {

    // normzlize user_id field and add timestamp 
    data['twitter_user_id']=data['user_id']; 
    delete data.user_id;
    data['refreshed'] = new Date();

    coll_twitter_userInfo.update({twitter_user_id: twitterUserId}, data, {upsert: true}, next);
  });
} 


/* 
* Check if any of your friends from twitter came to the system and suggest following 
* NOTE: pinterest doesn't interrupt you with this 
*       and has a pretty bug 'follow all button'
*/
var verifyNewFriends = function(userID, next) { 

} 


/* Passport session setup.
*   To support persistent login sessions, Passport needs to be able to
*   serialize users into and deserialize users out of the session.  Typically,
*   this will be as simple as storing the user ID when serializing, and finding
*   the user by ID when deserializing.  However, since this example does not
*   have a database of user records, the complete Twitter profile is serialized
*   and deserialized.
*
*   the objective of this is to return a real user object
*   this function gets called EVERYFUCKINGTIME , on every request
*   the objective is to turn a cookie into a real user object
*   so we shoudn't been accessing mongo from here
*
*   AKA : I don't fucking get this (pablo)
*   maybe this should play with the session store? 
*/
passport.serializeUser(function(user, done) {
  log.debug('serialization!!!', user);
  done(null, user);
});

passport.deserializeUser(function(obj, done) {
  log.debug('deserialization!!!', obj);
  done(null, obj);
});

/*  Simple route middleware to ensure user is authenticated.
*   Use this route middleware on any resource that needs to be protected.  If
*   the request is authenticated (typically via a persistent login session),
*   the request will proceed.  Otherwise, the user will be redirected to the
*   login page.
*/
function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) { return next(); }
  res.redirect('/login')
}

function ensureNotLoggedIn(req, res, next) {
  if (! req.isAuthenticated()) { return next(); }
  res.redirect('/')
}

var app = express.createServer();

// use moustache (hbs files) for the views
express.view.register('.hbs', hbs);

// pull index.html from the same folder
app.use(express.static(__dirname));

// use Redis as session store
var sessions = new RedisStore();

// configure Express
app.configure(function() { 
  app.use(express.static(__dirname + '../client'));
  app.set('views', __dirname + '/views');
  app.set('view engine', 'hbs');
  app.use(express.logger());
  app.use(express.cookieParser());
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(express.session({ secret: 'keyboard cat', store: sessions}));
  app.use(passport.initialize());
  app.use(passport.session());
  app.use(app.router);
});


// adding API routes 
// everything api is here
require('./api_routes.js')(app);


/**
 * Using information from providers, construct user object 
 * Several strategies are posible : first one wins, facebook (most reliable) wins, override information with user provided one, etc
 * Using first one wins here
 *
 * @param {Object} user
 */
app.get('/', ensureAuthenticated, function(req, res){
  // once you are authenticated, process to the frontend js application
  // this sends the user the view views/app/index.hbs 
  // which is the layout of the client backbone application 
  res.render('app', {layout : false});
});

app.get('/account', ensureAuthenticated, function(req, res){
  log.debug("hi from account" , req.user);
  res.render('account', { user: req.user });
});

app.get('/login', function(req, res){
  res.render('login', { user: req.user });
});

// test retrieve user object
app.get('/api/user', ensureAuthenticated, function(req, res){ 
  res.send(req.user);
})


/* 
  I am overloading the Facebook userProfile method since I want to load way more information from the user that what passportJS gives me
*/
FacebookStrategy.prototype.userProfile = function(accessToken, done) {
  this._oauth2.getProtectedResource('https://graph.facebook.com/me', accessToken, function (err, body, res) {
    if (err) { return done(err); }
    
    try {
      o = JSON.parse(body);
      
      var profile = { provider: 'facebook' };
      profile.id = o.id;
      profile.username = o.username;
      profile.displayName = o.name;
      profile.name = { familyName: o.last_name,
                       givenName: o.first_name,
                       middleName: o.middle_name };
      profile.gender = o.gender;
      profile.profileUrl = o.link;
      profile.emails = [{ value: o.email }];
      
      // added by me 
      profile.locale = o.locale;
      
      done(null, profile);
    } catch(e) {
      done(e);
    }
  });
}

passport.use('facebook', new FacebookStrategy({
    clientID: conf.fb.clientID,
    clientSecret: conf.fb.clientSecret,
    callbackURL: conf.fb.callbackURL, 
  },
  function(accessToken, refreshToken, profile, done) {
    profile.accessToken = accessToken; 
    profile.refreshToken = refreshToken;
    
    process.nextTick(function () {
      return done(null, profile);
    });
  }
));


// Actually twitter only gives id and screen_name as information from auth 
// https://dev.twitter.com/docs/auth/implementing-sign-twitter
TwitterStrategy.prototype.userProfile = function(token, tokenSecret, params, done) {
  var profile = { provider: 'twitter' };
  profile.id = params.user_id;
  profile.username = params.screen_name;
  // added by pablo
  profile.picture_url = 'https://api.twitter.com/1/users/profile_image?screen_name=' + params.screen_name + '&size=bigger'; // added by pablo (not part of the standard) 
  
  return done(null, profile);
}

passport.use('twitter', new TwitterStrategy({
    consumerKey: conf.twit.consumerKey,
    consumerSecret: conf.twit.consumerSecret,
    callbackURL: conf.twit.callbackURL, 
  },
  function(token, tokenSecret, profile, done) {
    profile.token = token;
    profile.tokenSecret = tokenSecret;
    process.nextTick(function () {
      return done(null, profile);
    });
  }
));

passport.use('google', new GoogleStrategy({
    clientID: conf.google.clientID,
    clientSecret: conf.google.clientSecret,
    callbackURL: conf.google.callbackURL, 
  },
  function(accessToken, refreshToken, profile, done) {
    profile.accessToken = accessToken; 
    profile.refreshToken = refreshToken;
    
    process.nextTick(function () {
      return done(null, profile);
    });
  }
));

// GET /auth/twitter
//   Use passport.authenticate() as route middleware to authenticate the
//   request.  The first step in Twitter authentication will involve redirecting
//   the user to twitter.com.  After authorization, the Twitter will redirect
//   the user back to this application at /auth/twitter/callback
app.get('/auth/twitter',
//  ensureNotLoggedIn,
  passport.authenticate('twitter'),
  function(req, res){
  });

// GET /auth/facebook
//   Use passport.authenticate() as route middleware to authenticate the
//   request.  The first step in Facebook authentication will involve
//   redirecting the user to facebook.com.  After authorization, Facebook will
//   redirect the user back to this application at /auth/facebook/callback
app.get('/auth/facebook',
//  ensureNotLoggedIn,
  passport.authenticate('facebook'),
  function(req, res){
  });

app.get('/auth/google',
  passport.authenticate('google', { scope: ['https://www.googleapis.com/auth/userinfo.profile',
                                            'https://www.googleapis.com/auth/userinfo.email', 
                                            'https://www.googleapis.com/auth/latitude.current.best', 
                                            'https://www.googleapis.com/auth/plus.me', 
] }), function(req, res){
  });


app.get('/auth/:provider/callback', function(req, res, next) {
  console.log('provider callback');
  passport.authenticate(req.param('provider'), { failureRedirect: '/login' }, function(err, user) {
    if (err) { return next(err) }
    if (!req.isAuthenticated()) {


     var newUser; 

     // user is not authenticated, go ahead and authenticate them
 
      var u = new User();

      u.on('logged_in', function(data) { 
        req.logIn({'username': data.username, providers: data.providers, '_id': data._id.toString()}, function(err) {
          console.log('req.login', err, data);
          if (err) { return next(err); }
          console.log('ok!');
          return res.redirect('/account');
        });
      });


      u.on('new_user', function(data) { 
         console.log('new user')
         req.logIn({'username': data.username, providers: data.providers, '_id': data._id.toString()}, function(err) {
          if (err) { return next(err); }
          return res.redirect('/');
        });
      });

     u.populate(user.provider, user.id, user);

    } else {

      // user is already authenticated, this must be a "linked" account
      // req.user is the existing user, and user is the info from the "linking"
      // provider.  Associate it however you please

      console.log('not auth!!!!');
      log.debug('user already auth', req.user);

      var u = new User(req.user);
      u.addProvider(req.param('provider'), user); 

      u.on('new_provider', function(data) { 
        log.debug('new provider', data);
        req.user = data;
        res.redirect('/account');
      })
    }
  })(req, res, next);
});

app.get('/deauth/:provider', function(req, res) { 
  // delete provider 
  // if there is only one provider, don't allow the user to do that (user object will remain in an unconsistent state) 
  var u = new User(req.user);
  u.removeProvider(req.params['provider']);
  u.on('provider_removed', function(data) { 
      req.user = data;
      res.redirect('/account')
  });
  u.on('last_provider', function(data) { 
      res.redirect('/login')
  });
});

app.get('/logout', function(req, res){
  req.logout();
  res.redirect('/');
});

// add friends from twitter
app.get('/friends/:provider', ensureAuthenticated, function(req, res){ 
  if (req.user['providers'][req.param('provider')]) { 
    log.debug('checking twitter friends');
    addFriendsFromProvider[req.param('provider')](req.user.providers[req.param('provider')].id, function() { log.debug('done retrieving ' + req.param('provider') + ' friends') });
    res.send('friends added');
  } else { 
    req.send('error: user does not have provider ' + req.param('provider'));
  } 
});



// test: send a message to all clients 
app.get('/message/test', ensureAuthenticated, function(req, res){ 
  log.debug('sending message');
  messages.emit('created', { id: 'myid', text: 'bar, visita nuestro bar' });
  res.send('ok');
}) 

app.get('/notification/test', ensureAuthenticated, function(req, res){ 
  log.debug('sending notification');
  notifications.emit('created', { id: 'myid', text: 'this is a notification' });
  res.send('ok');
}) 



var messages = backboneio.createBackend();
var events = backboneio.createBackend();
var session = backboneio.createBackend();
var users = backboneio.createBackend();
var notifications = backboneio.createBackend();

// auth for sockets 
var auth = function(req, res, next) {
    if (!req.session.passport.user) {
        log.debug('unauthorized by ', req.session.passport.user);
        next(new Error('Unauthorized'));
    } else {
        next();
    }
};

[messages, events, session , users, notifications].forEach(function(backend) {
  backend.use(backboneio.middleware.cookieParser());
  backend.use(my_session_middleware({ store: sessions }));
  backend.use('create', 'update', 'delete', auth);
  // TODO: put a middleware to make sure the user has access to the object 
});


// populate stuff , before loading the server
messages.use(my_mongo_middleware(coll_messages));  
events.use(my_mongo_middleware(coll_events)); 
session.use(static_object_middleware());    // user should return current user here  -> TODO -> this is somehow NOT working
users.use(my_mongo_middleware(coll_user));  
notifications.use(my_mongo_middleware(coll_notifications));  

var socket = backboneio.listen(app, { messages: messages, events: events, users: users, session: session });

// comment this line below to see a lot of very verbose logs from socket.io 
socket.set('log level', 1);
socket.sockets.on('connection', function(client) { 
  console.log('connected');
  client.on('message', function(message_json) {
      var message = JSON.parse(message_json);
      console.log(message);
      if(message.key) {
        console.log('sending', message.key);
        console.log(socket.sockets.clients);
        socket.sockets.clients[message.key].send('Hello world');
      }
  });
});
app.listen(3000);
