module.exports = function(coll) {

    return function(req, res, next) {
        var callback = function(err, result) {
            if (err) return next(err);
            res.end(result);
        };
        
        var crud = {
            create: function() {
                //console.log('creating', req.model, req);

                req.model.owner = req.session.user; // add owner by default 
                req.model.date = new Date();

                coll.insert(req.model, callback); 
            },
            
            read: function() {
                console.log('read', req.model);
                if (req.model._id) {
                    coll.findById(req.model._id, callback); 
                } else {
                    coll.find().toArray(function(err, result) { console.log('find') ;  callback(err, result) }); 
                }
            },
            
            update: function() {
                var model = {};
                for (var key in req.model) {
                    model[key] = req.model[key];
                }
                delete model._id;
                
                coll.update({ _id: req.model._id}, {'$set': model},  function(err) { 
                    if (err) return next(err);
                    res.end(req.model);
                });
            },
            
            delete: function() {
                coll.remove({ _id: req.model._id }, function(err) {
                    if (err) return next(err);
                    res.end(req.model);
                });
            }
        };
        
        if (!crud[req.method]) return next(new Error('Unsuppored method ' + req.method));
        //console.log("I am inside", req.method, req.model);
        crud[req.method]();
    }
};
