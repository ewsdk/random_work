module.exports = function(options) {
    options || (options = {});
    options.key || (options.key = 'connect.sid');
    
    //console.log('inside session1', options.store);
    if (!options.store) throw new Error('No session store provided');
    
    return function(req, res, next) {
//        console.log('inside session', req);
        req.sessionID = req.cookies[options.key];
 //       console.log('session', req.sessionID, req.cookies[options.key]);
        
        if (req.sessionID) {
            options.store.get(req.sessionID, function(err, session) {
                if (err) return next(err);
                req.session = session;
                next();
            });
        } else {
            next();
        }
    };
};
