var log = require('winston'), 
  config = require('conf');

require('winston-mongodb').MongoDB;

log.add(log.transports.Loggly, config.log.loggly);
log.add(log.transports.MongoDB, {safe: false, level: 'debug', db: 'log'});

log.debug('starting all this!')

exports.log = log;
