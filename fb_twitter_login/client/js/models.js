define(['logger'], function(log) { 

  var Models = {};

  Models.Message = Backbone.Model.extend({
    initialize: function() {
      this.bind('error', function(model, err) {
          log.debug(err.message, model);
      });
    }
  });

  Models.NotificationModel = Backbone.Model.extend({
    initialize: function() {
      this.bind('error', function(model, err) {
          log.debug(err.message, model);
      });
    }
  });

  Models.Ride = Backbone.Model.extend({
    initialize: function(rideID) {
      this.bind('error', function(model, err) {
          log.debug(err.message, model);
      });
//      this.backend = { name: 'ride', channel: rideID }
      this.backend = 'ride';
      var self = this;
      this.bind('backend:create', function(model) {
          log.debug('ride ', model);
      });
      this.bind('backend:update', function(model) {
          cosole.log('received update', model);
      //    self.get(model.id).set(model);
      });
    }
  });


  Models.UserModel = Backbone.Model.extend({ 
    backend: 'session',
    initialize: function() {
      // Setup default backend bindings
      log.debug('user init', this);
      // (see lib/browser.js for details).
      var self = this;

      this.bind('backend:create', function(model) {
          log.debug('received create', model);
          self.add(model);
      });
      this.bind('backend:update', function(model) {
          cosole.log('received update', model);
          self.get(model.id).set(model);
      });
      this.bind('backend:delete', function(model) {
          cosole.log('received delete', model);
          self.remove(model.id);
      });
    }
  });


  Models.Messages = Backbone.Collection.extend({
    // Specify the backend with which to sync
    backend: 'messages',
    model: Models.Message,
    initialize: function() {
      // Setup default backend bindings
      log.debug('messages init', this);
      // (see lib/browser.js for details).
      var self = this;

      this.bind('backend:create', function(model) {
          log.debug('received create', model);
          self.add(model);
      });
      this.bind('backend:update', function(model) {
          cosole.log('received update', model);
          self.get(model.id).set(model);
      });
      this.bind('backend:delete', function(model) {
          cosole.log('received delete', model);
          self.remove(model.id);
      });
    }
  });

  Models.Users = Backbone.Collection.extend({
    // Specify the backend with which to sync
    backend: 'users',
    model: Models.UserModel,
    initialize: function() {
      // Setup default backend bindings
      log.debug('individual user init', this);
      // (see lib/browser.js for details).
      //this.bindBackend();
      this.bind('backend:create', function(model) {
          log.debug('received create', model);
          self.add(model);
      });
      this.bind('backend:update', function(model) {
          cosole.log('received update', model);
          self.get(model.id).set(model);
      });
      this.bind('backend:delete', function(model) {
          cosole.log('received delete', model);
          self.remove(model.id);
      });
    }
  });

  Models.Following = Backbone.Collection.extend({ 
    model: Models.UserModel,
    url: 'http://laptop.folksonomy.com:3080/1/following',
    initialize: function() { 
    } 
  });

  Models.Followers = Models.Following.extend({ 
    url: 'http://laptop.folksonomy.com:3080/1/followers'
  });

  Models.Notifications = Backbone.Collection.extend({
    // Specify the backend with which to sync
    backend: 'notifications',
    model: Models.NotificationModel,
    initialize: function() {
      var self = this;
      this.bind('backend:create', function(model) {
          log.debug('notification received create', model);
          self.add(model);
      });
      this.bind('backend:update', function(model) {
          cosole.log('notification received update', model);
          self.get(model.id).set(model);
      });
      this.bind('backend:delete', function(model) {
          cosole.log('notification received delete', model);
          self.remove(model.id);
      });
    }
  });


  return Models;
});
