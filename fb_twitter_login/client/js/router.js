define(['models', 'views', 'logger'], function(Models, Views, log) { 

  var Workspace = Backbone.Router.extend({
    mode: null, 
    messages: new Models.Messages(),   // this is fetched from the views
    usersFollowing: new Models.Users(),  // this is fetched from the views
    user: new Models.UserModel(),
    notifications : new Models.Notifications(),

    initialize: function() { 
    },

    routes: {
      "":                 "homepage",   
      "account" :         "account",
      "following" :       "showFollowing",
      "followers" :       "showFollowers",
      "messages" :        "showMessages",
      "users" :           "showUsers",
      "user/:username":   "showUser",  
      "ride/:rideID":     "showRide",  
      "logout":           "logout",
    },

    initialize: function() { 
      // TODO : check why this doesn't work
      //this.messages.fetch();
      //this.notifications.fetch();
      log.debug('initialize');
    },

    homepage: function() {
      $('#container').empty();

      // load homepagelayout 
      new Views.HomePageView({ el: $('#container')}).render();

      var that =this;
      // user is not fetched from the views
      this.user.fetch({success: function(data){ 
        log.debug('user fetched', data);
        new Views.UserPersonalView({ el: $('#my_user'), model: that.user}).render();
      }});

      //collection level views fetch the elements themselves
//      new Views.MessagesView({ el: $('#messages'), collection: this.messages });
//      new Views.UsersView({ el: $('#userlist'), collection: this.usersFollowing});
      new Views.NotificationsView({ el: $('#notifications'), collection: this.notifications});
    },

    showMessages: function() { 
      this.messages.fetch();
      $('#container').empty();
      $('#container').html('Messages <div id="messages"></div>');
      new Views.MessagesView({ el: $('#messages'), collection: this.messages });
    },

    /* 
    * Show who the user is following and who is following the user
    * this.users is a general 'module' level user list with all the users friends
    * this may change in the future, specially since it's not needed to have all that all the time synchromizing back and forth
    */
    showFollowing: function(){ 
      $('#container').empty();
      $('#container').html('Following <div id="userlist"></div>');
      new Views.UsersView({ el: $('#userlist'), collection: this.usersFollowing});
    }, 

    showFollowers: function(){ 
      var that=this;
      $('#container').empty();
      $('#container').html('Followers <div id="userlist"></div>');
      var followers = new Models.Users();
      followers.fetch({'success': function(data) { 
        log.debug('followers', data);
        new Views.UsersView({ el: $('#userlist'), collection: followers});
      }})
    },

    showUsers: function(){ 
      $('#container').empty();
      $('#container').html('Users <div id="userlist"></div>');
      new Views.UsersView({ el: $('#userlist'), collection: this.usersFollowing});
    }, 

    showRide: function(rideID) { 
      log.debug('showing ride');
      $('#container').empty();
      var ride = new Models.Ride(rideID);
      new Views.RidePageView({ el: $('#container'), model: ride}).render();
    },

    showUser: function(username) {
      console.log('user', username);
      $('#container').empty();
      var user = new Models.User( )
      /*
      * showing messages from the user - uncomment to work on this
      this.messges = new Models.Messages({'user': username});
      this.messages.fetch({'user': username})
      this.messages.user = username;
      new Views.MessagesView({ el: $('#messages'), collection: this.messages });
      */
    }, 

    /* 
      Allow change account, etc
    */
    account: function() { 

    }, 

    defaultRoute: function() { 
      log.debug('404!');
    }
  });

  return { Workspace: Workspace } ;
});

  
