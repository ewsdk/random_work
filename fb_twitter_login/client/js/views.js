define(['logger', 'handlebars', 'models', 'unbstrusive-google-maps.jquery', 'text!/templates/HomePage.html', 'text!/templates/UsersView.html', 'text!/templates/MessagesView.html','text!/templates/MessageView.html', 'text!/templates/UserView.html', 
  'text!/templates/UserPersonalView.html', 'text!/templates/Ride.html'], 
  function(log, _handlebars, Models, _map, HomePageTemplate, UsersViewTemplate, MessagesViewTemplate, MessageViewTemplate, UserViewTemplate, UserPersonalViewTemplate, RideViewTemplate) { 

  var Views = {} 

  Views.HomePageView = Backbone.View.extend({ 
    initialize: function() {
        this.compiledTemplate = Handlebars.compile(HomePageTemplate);
    },
    render: function() { 
        $(this.el).append(this.compiledTemplate());
    } 
  });

  Views.MessageView = Backbone.View.extend({
      
    tagName: 'li',
    
    events: {
        'click .delete': 'delete'
    },
    
    initialize: function() {
        _.bindAll(this, 'render', 'delete');
        this.compiledTemplate = Handlebars.compile(MessageViewTemplate);
    },
    
    render: function() {
        $(this.el).append(this.compiledTemplate(this.model.toJSON()));
        return this;
    },
    
    delete: function(e) {
        console.log('delete', this.model);
        e.preventDefault();
        this.model.destroy();
    }
      
  });

  Views.UserView = Backbone.View.extend({
      
    tagName: 'li',
    
    events: {
        'click .delete': 'delete',
        'click .ping': 'ping'
    },
    
    initialize: function() {
        _.bindAll(this, 'render', 'delete');
        this.compiledTemplate = Handlebars.compile(UserViewTemplate);
    },
    
    render: function() {
        $(this.el).append( this.compiledTemplate(this.model.toJSON()) );
        return this;
    },
    
    delete: function(e) {
        console.log('delete', this.model);
        e.preventDefault();
        this.model.destroy();
    }, 

    // send ping to user and show in its notifications
    ping : function(e) { 
        console.log('ping', this.model);
        e.preventDefault();
        // TODO : send ping here 
        var m = new Models.Message();
        m.set({text: 'ping'});
    } 
      
  });

  Views.UsersView = Backbone.View.extend({ 

    initialize: function() {
        _.bindAll(this, 'render');

        this.compiledTemplate = Handlebars.compile(UsersViewTemplate);
        this.collection.bind('add', this.render);
        this.collection.bind('change', this.render);
        this.collection.bind('remove', this.render);
        this.collection.bind('reset', this.render);
        this.collection.fetch(function(data) { console.log('data', data)});
    },
    
    render: function() {

        $(this.el).append( this.compiledTemplate() );  // placeholder
        log.debug('rendering user list', this.collection);
        this.collection.each(function(user) {
            console.log('user', user);
            var view = new Views.UserView({ model: user});
            this.$('.userlist ul').append(view.render().el);
        });
        
        return this;
    },
  });

  Views.MessagesView = Backbone.View.extend({
  
    events: {
        'click .send': 'send',
        'keypress .message': 'keypress'
    },

    initialize: function(options) {
        _.bindAll(this, 'render', 'send', 'keypress');
    
        this.collection.bind('add', this.render);
        this.collection.bind('change', this.render);
        this.collection.bind('remove', this.render);
        this.collection.bind('reset', this.render);

        this.compiledTemplate = Handlebars.compile(MessagesViewTemplate);
//                this.collection.fetch();
        
    },

    render: function() {
        $(this.el).append( this.compiledTemplate() );  // placeholder for individual messages
        
        this.collection.each(function(message) {
            var view = new Views.MessageView({ model: message });
            this.$('.messageslist ul').append(view.render().el);
        });
        
        return this;
    },

    send: function() {
        this.collection.create({ text: this.$('.message').val() });
        console.log('sending');
        this.$('.message').val('');
    },
    
    keypress: function(e) {
        if (e.which === 13) this.send();
    }
  
  });


  Views.UserPersonalView = Backbone.View.extend({
    tagName: 'li',
    events: {
        //'click .delete': 'delete'
    },
    initialize: function() {
        _.bindAll(this, 'render');
        this.compiledTemplate = Handlebars.compile(UserPersonalViewTemplate);
    },
    render: function() {
        console.log('rendering ', this.model.toJSON());
        $(this.el).append( this.compiledTemplate(this.model.toJSON()) );
        return this;
    },
  });


  Views.RidePageView = Backbone.View.extend({
    initialize: function() {
        this.compiledTemplate = Handlebars.compile(RideViewTemplate);
    },
    render: function(rideID) { 
        $(this.el).append(this.compiledTemplate(this.model.toJSON()));
    }
  });


  Views.NotificationsView = Backbone.View.extend({ 
    tagName: 'li',
    events: {
        //'click .delete': 'delete'
    },
    initialize: function() {
        _.bindAll(this, 'render');
        log.debug('init notification');
    },
    render: function() {
        console.log('rendering notification yo!!!', this.model.toJSON());
    },

  });
/*
  var Views={}; 

  Views.home = Backbone.View.extend({ 
    el: $('#container'),
    initialize: function() { 
      this.compiledTemplate = Handlebars.compile(homeTemplate);
      log.debug('template', this.compiledTemplate);
    },
    render: function(){
      var data = {};
      // Append our compiled template to this Views "el"
      $(this.el).append( this.compiledTemplate(data) );
    }
  });
  Views.user = Backbone.View.extend({ 
    el: $('#container'),
    initialize: function() { 
      this.compiledTemplate = Handlebars.compile(userTemplate);
      this.render = _.bind(this.render, this);
      this.model.bind('change', this.render, this);
    },
    render: function() { 
      console.log('user view', this.model.toJSON());
      $(this.el).empty().append(this.compiledTemplate(this.model.toJSON()));
    }  
  });
 */ 

  return Views;
});
