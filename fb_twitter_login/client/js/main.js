(function () {
  'use strict';   
     
  var config = {}, underscore, bootstrap;

  config.baseUrl = './js/';
  // path to the modules
  // I am going to keep urls pointing to particular versions to our sandboxes are stable
  // for some peculiarity it looks like you can't put the trailing extension (.js) on the files
  // (t's a bug, definitely)
  config.paths = {
    'unbstrusive-google-maps.jquery': "https://raw.github.com/augustl/unobtrusive-google-maps/master/unobtrusive-google-maps.jquery",
    'underscore': 'https://raw.github.com/amdjs/underscore/master/underscore-min',
    'backbone': "lib/backbone",
    'log4javascript': 'lib/log4javascript',
    'handlebars': 'lib/handlebars.min',
    'socket.io': 'http://cdnjs.cloudflare.com/ajax/libs/socket.io/0.9.5/socket.io.min',
    'jquery.cookie': 'https://raw.github.com/carhartl/jquery-cookie/master/jquery.cookie',
    'backbone.io': 'lib/browser',  // needs Backbone on global space
  };
     
  underscore = require({ context : 'underscore', baseUrl: config.baseUrl, paths: config.paths });
  bootstrap = require({ context : 'bootstrap',  baseUrl: config.baseUrl, paths : config.paths });
  underscore(['order!underscore', 'order!backbone', 'order!socket.io'], function (_, _backbone, io) {

    window._ = _; // move underscore to global namespace for backbone
    window.Backbone = _backbone ; // browser.js needs this on the global namespace 
    io = io; // browser.js req

    bootstrap(['application', 'logger','backbone.io',], function(app, log) { 
    
      window.app= {} ; // global namespace TODO : I am not using this 
      window.app.conf = { pushState: false} ; 
     
      // with backbone.io we should not need this , but it may be nice to have a shared socket for all the other non backbone.io needs
      ///window.app.socket = io.connect('http://localhost:3000');

      // this is imported from a modification I made in browser.js , which links the socket to the global window object (April 2012) 
      window.socket.on('connect', function(){
        socket.send({key: socket.transport.sessionid});
      });
      window.socket.on('message', function(data){
        console.log('received', data);
      });
      window.socket.on('disconnect', function(){
        console.log('disconnected');
      }); 

      window.w = new app.Workspace();
      Backbone.history.start({ pushState: window.app.conf.pushState });
      log.debug('running', w.routes);

      // read : https://github.com/documentcloud/backbone/issues/397

      // https://gist.github.com/1142129
      // hijack links for pushState in Backbone

      // also read: http://evanprodromou.name/2012/01/05/on-using-backbone-router-with-pushstate/

      // TODO break links 

      // Use absolute URLs  to navigate to anything not in your Router.

      // Only need this for pushState enabled browsers
      if (Backbone.history && Backbone.history._hasPushState) {

        log.debug("pushState enable, I'll capture a-href clicks");

        // Use delegation to avoid initial DOM selection and allow all matching elements to bubble
        // visual doesn't work, live does
        $(document).on("click", "a", function(e) {
          log.debug('click!');
          // Get the anchor href and protcol
          var href = $(this).attr("href");
          var protocol = this.protocol + "//";

          if (!window.app.con.pushState) { href = "#"+href }
          alert('in pushstate', href, protocol);

          // Ensure the protocol is not part of URL, meaning its relative.
          // Stop the event bubbling to ensure the link will not cause a page refresh.
          if (href.slice(protocol.length) !== protocol) {
            e.preventDefault();

            // TODO remove #! if there is any (to assure compatibility with hash bangs

            // Note by using Backbone.history.navigate, router events will not be
            // triggered.  If this is a problem, change this to navigate on your
            // router.
            Backbone.history.navigate(href, true);
          }
        });
      }

    log.debug('everything loaded!', app);
    })
    });   
}());

