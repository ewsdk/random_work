// define all logging here

define(['log4javascript'], function(_log4javascript) { 
  var log = log4javascript.getLogger();
  
  var browserAppender = new log4javascript.BrowserConsoleAppender();
  browserAppender.setThreshold(log4javascript.Level.DEBUG);
  log.addAppender(browserAppender);

  log.debug("this is debugging message");

  return log; 
});
